# daverona/compose/mantisbt

## Prerequisites

* `mantisbt.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/compose/traefik) out

## Quick Start

```bash
cp .env.example .env
# edit .env
docker-compose up --detach
```

Wait until mantisbt is up and visit [http://mantisbt.example](http://mantisbt.example).
The default username and password is `administrator` and `root` respectively.

## References

* MantisBT documentation: [https://www.mantisbt.org/](https://www.mantisbt.org/)
* Admin Guide: [https://www.mantisbt.org/docs/master/en-US/Admin\_Guide/html-desktop/](https://www.mantisbt.org/docs/master/en-US/Admin_Guide/html-desktop/)
* MantisBT Plugins: [https://www.mantisbt.org/wiki/doku.php/mantisbt:plugins:start](https://www.mantisbt.org/wiki/doku.php/mantisbt:plugins:start)
* MantisBT repository: [https://github.com/mantisbt/mantisbt](https://github.com/mantisbt/mantisbt)
